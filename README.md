# Requirements

* python3
* pip3
* pipenv

# Install pipenv

Please check https://docs.pipenv.org/en/latest/install/#installing-pipenv for you specific operating system.

# Install dependend libraries

You need run install the libraries once. On changes of the Pipfile run again:

```
$ pipenv install
```

# Start

To start the program just run `./start.sh` on Linux/macOS or `start.bat` on Windows.
