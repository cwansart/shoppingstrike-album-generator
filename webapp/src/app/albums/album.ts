export interface AlbumContainer {
  readonly name: string;
  readonly images: Album[];
}

export interface Album {
  readonly path: string;
  readonly description?: string;
}
