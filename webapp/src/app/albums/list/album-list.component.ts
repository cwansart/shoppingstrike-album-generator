import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { AlbumContainer } from '../album';
import { AlbumService } from '../album.service';

@Component({
  templateUrl: './album-list.component.html',
})
export class AlbumListComponent {

  public albums: Observable<AlbumContainer[]>;

  constructor(service: AlbumService) {
    this.albums = service.getAllAlbums();
  }
}
