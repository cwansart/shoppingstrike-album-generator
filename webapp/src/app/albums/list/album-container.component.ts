import { Component, Input } from '@angular/core';
import { Album } from '../album';

@Component({
  selector: 'app-album-container',
  templateUrl: './album-container.component.html',
})
export class AlbumContainerComponent {

  @Input()
  public name: string;

  @Input()
  public images: Album[];
}
