import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AlbumContainer } from './album';

@Injectable({
  providedIn: 'root'
})
export class AlbumService {

  constructor(private http: HttpClient) {
  }

  public getAllAlbums(): Observable<AlbumContainer[]> {
    return this.http.get<AlbumContainer[]>('/albums.json');
  }
}
