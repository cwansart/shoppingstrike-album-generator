import { Component } from '@angular/core';
import { AlbumService } from './albums/album.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  title = 'webapp';
}
