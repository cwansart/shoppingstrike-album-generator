from sys import argv, exit
from os import listdir
from os.path import isdir
import json
import subprocess

if len(argv) < 3:
    print('Too few arguments. Please specify the path to the albums and the path')
    print('relative to webroot. The syntax is as follows:')
    print(' {} <pathToAlbums> <relativePathToAlbumsFromWebroot>'.format(argv[0]))
    print('\nFor example:')
    print(' {} "C:\\shoppingstrike\\data\\img\\albums" "./data/img/albums"'.format(argv[0]))
    exit(1)

root_dir = argv[1]
print('Use root_dir: {}'.format(root_dir))

if not isdir(root_dir):
    print('{} does not exist or is not a directory. Please check the given path'.format(root_dir))
    exit(1)

folders = [file for file in listdir(root_dir) if isdir('{}/{}'.format(root_dir, file))]
print('Found album folders: {}'.format(folders))

albums = []
for folder in folders:
    folder_path = '{}/{}'.format(root_dir, folder)
    album_files = [album_folder for album_folder in listdir(folder_path) if isdir(folder_path)]
    albums.append({'name': folder,
                   'images': list(map(lambda file: {'path': '{}/{}/{}'.format(argv[2], folder, file)}, album_files))})
    print('Found files in {}: {}'.format(folder, album_files))

print('Albums: {}'.format(albums))
with open('./webapp/src/albums.json', 'w', encoding='utf-8') as albums_json:
    json.dump(albums, albums_json, indent=2)

# check if npm install has run on the webapp, if not run it
if not isdir('./webapp/node_modules'):
    subprocess.check_call('npm install --prefix webapp', shell=True)

# start the webapp
subprocess.check_call('npm start --prefix webapp', shell=True)
